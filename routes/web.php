<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'WelcomeController');

Route::get('/productos', function () {
    return 'Listado de productos';
});

Route::get('/productos/{id}/{caption?}', function ($id, $caption = '') {
    $salida = "Información especifica del producto {$id}";
    $salida .= empty($caption) ? '' : " con {$caption}";

    return $salida;
});


Route::get('/clientes', 'ClientesController@index');

Route::get('/clientes/{id}', 'ClientesController@detalle')
    ->where('id', '\d+');



Route::get('/reportes', function () {

    return "Listado de reportes";
});

Route::get('/reportes/nuevo', function () {

    return "Crear nuevo reporte";
});

Route::get('/reportes/{fecha}', function ($fecha) {

    return "Reporte del dia {$fecha}";
});
