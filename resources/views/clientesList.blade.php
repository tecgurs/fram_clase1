
@empty ($clientes)

    <h5>No hay clientes</h5>

@else

    @foreach ($clientes as $cliente)

        @if ($loop->first)
            <table>
            <tr>
                <th>Nombre</th>
                <th>Edad</th>
            </tr>
        @endif

        <tr>
            <td>{{ $cliente['nombre'] }}</td>
            <td>{{ $cliente['edad'] }}</td>
        </tr>

        @if ($loop->last)
           </table>
        @endif

    @endforeach

@endif