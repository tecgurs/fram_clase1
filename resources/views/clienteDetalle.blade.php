@empty($nombre)

    <h5>No existe el cliente</h5>

@else

    <table>
        <tr>
            <th>Nombre</th>
            <th>Edad</th>
        </tr>
        <tr>
            <td>{{ $nombre }}</td>
            <td>{{ $edad }}</td>
        </tr>
    </table>

@endif
