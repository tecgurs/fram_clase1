<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientesController extends Controller
{
    private $clientes = [
        0 => [
            'nombre' => '',
            'edad' => '',
        ],
        23 => [
            'nombre' => 'Alberto Alvarez',
            'edad' => 28,
        ],
        24 => [
            'nombre' => 'Benjamín Bardales',
            'edad' => 48,
        ],
    ];

    function index()
    {
        $clientes = $this->clientes;
        unset($clientes[0]);
        //return view('clientesList', ['clientes' => $clientes]);
        return view('clientesList')
            ->with('clientes', $clientes);
    }

    function detalle($id)
    {
        if (key_exists($id, $this->clientes)) {
            $cliente = $this->clientes[$id];
        } else {
            $cliente = $this->clientes[0];
        }
        //return view('clienteDetalle', $cliente);
        return view('clienteDetalle')
            ->with('nombre', $cliente['nombre'])
            ->with('edad', $cliente['edad']);
    }
}
